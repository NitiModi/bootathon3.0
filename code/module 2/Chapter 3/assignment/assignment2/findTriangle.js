function whichTriangle() {
    var side1 = document.getElementById("side1");
    var side2 = document.getElementById("side2");
    var side3 = document.getElementById("side3");
    var p = document.getElementById("display");
    var a = parseFloat(side1.value);
    var b = parseFloat(side2.value);
    var c = parseFloat(side3.value);
    if (a == b && b == c) {
        p.innerHTML += "It is an isoceles triangle ";
    }
    else if (a == b || b == c || c == a) {
        alert("It is an isoceles triangle");
    }
    else {
        alert("It is an scalene triangle");
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) ||
            Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) ||
            Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            alert("It is an right angle triangle");
        }
    }
}
//# sourceMappingURL=findTriangle.js.map