function promptfn() {
    var n = document.getElementById("n");
    var i;
    var positive = 0;
    var negative = 0;
    var zero = 0;
    for (var i = 0; i < Math.trunc(+n.value); i++) {
        var x = prompt();
        var value = +x;
        if (value > 0) {
            ++positive;
        }
        else if (value < 0) {
            ++negative;
        }
        else {
            ++zero;
        }
    }
    alert("Total positive values:" + positive);
    alert("Total negative values:" + negative);
    alert("Total zeros:" + zero);
}
//# sourceMappingURL=prompt.js.map