function area() {
    var x1 = document.getElementById("t11");
    var y1 = document.getElementById("t12");
    var x2 = document.getElementById("t21");
    var y2 = document.getElementById("t22");
    var x3 = document.getElementById("t31");
    var y3 = document.getElementById("t32");
    var ans = document.getElementById("ans");
    var A = Math.sqrt(Math.pow(+x2.value - +x1.value, 2) + Math.pow(+y2.value - +y1.value, 2));
    var B = Math.sqrt(Math.pow(+x3.value - +x2.value, 2) + Math.pow(+y3.value - +y2.value, 2));
    var C = Math.sqrt(Math.pow(+x3.value - +x1.value, 2) + Math.pow(+y3.value - +y1.value, 2));
    var S = (A + B + C) / 2;
    var area = Math.sqrt(S * (S - A) * (S - B) * (S - C));
    ans.value = area.toString();
}
//# sourceMappingURL=areaOfTriangle.js.map