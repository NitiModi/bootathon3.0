function addToScreen(val) {
    var screen = document.getElementById("screen");
    screen.value += val;
}
function calculate() {
    var screen = document.getElementById("screen");
    var expr = screen.value;
    //if given input does not start with number it is one of the scientific operation
    if (isNaN(+expr.charAt(0))) {
        var expr1 = expr.substring(0, 3);
        var num = +expr.slice(3);
        var ans1;
        //if first char is s
        if (expr1.charAt(0) == 's') {
            ans1 = Math.sin(num);
            screen.value = ans1.toString();
        }
        //if first char is c
        else if (expr1.charAt(0) == 'c') {
            ans1 = Math.cos(num);
            screen.value = ans1.toString();
        }
        //if first char is t
        else if (expr1.charAt(0) == 't') {
            ans1 = Math.tan(num);
            screen.value = ans1.toString();
        }
    }
    //if it is a number
    else if (expr.indexOf('^') != -1) {
        var splitted = expr.split("^");
        var num1 = +splitted[0];
        var num2 = +splitted[1];
        screen.value = Math.pow(num1, num2).toString();
        //alert(splitted);
    }
    else {
        var ans = eval(expr);
        screen.value = ans.toString();
    }
}
function clearScreen() {
    var screen1 = document.getElementById("screen");
    var a = "";
    screen1.value = a;
}
function sqrt() {
    var screen = document.getElementById("screen");
    var num = +screen.value;
    screen.value = Math.sqrt(num).toString();
}
function power() {
    var screen = document.getElementById("screen");
    var num = +screen.value;
    screen.value = Math.sqrt(num).toString();
}
//# sourceMappingURL=scientificCalc.js.map